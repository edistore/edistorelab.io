+++
title = "Disclaimer"
description = "Disclaimer"
date = "2018-03-30T10:34:07+07:00"
slug = "disclaimer"
layout = "tentang-kami"
+++

Hasil positif, testimonial, foto, dan gambar yang terdapat di website tidak menjadi gambaran atau jaminan bahwa Anda akan memperoleh hasil yang sama. Kami tidak bertanggungjawab apabila hasil yang diperoleh tidak sesuai dengan harapan Anda, karena hasil yang diperoleh pada setiap orang bisa berbeda-beda.

Kami juga tidak bertanggungjawab atas apapun yang terjadi akibat menggunakan produk yang Anda beli, termasuk dampak serta efek samping yang dialami. Kami berusaha menginformasikan produk dan hasil yang bisa diperoleh dengan wajar dan tidak berlebihan.

Terima kasih.