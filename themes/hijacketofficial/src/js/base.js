/**
 *  Hijacket.net Theme - v1.0.0 - Main JavaScript file
 */

var hijacket = (function ($) {

    'use strict';
    var _slickSlider = function () {
            // SLICK
            $('.slider-for').slick({
                slidesToShow: 1,
                slidesToScroll: 1,
                arrows: false,
                fade: true,
                asNavFor: '.slider-nav'
            });
            $('.slider-nav').slick({
                slidesToShow: 5,
                slidesToScroll: 1,
                asNavFor: '.slider-for',
                dots: true,
                focusOnSelect: true,
            });

            // ZOOM
            $('.ex1').zoom();

            // STYLE GRAB
            $('.ex2').zoom({ on:'grab' });

            // STYLE CLICK
            $('.ex3').zoom({ on:'click' });	

            // STYLE TOGGLE
            $('.ex4').zoom({ on:'toggle' });
        },
        _mobileMenu = function () {
            $(".nav-menu__item li").hover(
            function(){
            //i used the parent ul to show submenu
            $(this).children('ul').slideDown('fast');
            }, 
            //when the cursor away 
            function () {
            $('ul', this).slideUp('fast');
            });
            
            $('.nav-toggle').on('click', function () {
                $('.nav-menu').toggleClass('nav-menu-active');
            });
            // Hamburger to X toggle
            $('.nav-toggle').on('click', function () {
                this.classList.toggle('active');
            });
        },
         _clickOrder = function () {
            $(".order-now").click(function(){
                $(".product-description, .related-product").toggle(200,'linear');
                $(".product-form").toggle(400,'linear');
            });
        },
        _selectPrice = function () {
            var valSale = priceSale();
            var valBuy = valSale - 20000;
            
            $("#product-price__buy").text(formatAngka(valBuy));
            $("#product-price__sale").text(formatAngka(valSale));
            function priceSale() {
                var valSale = parseInt($('#product-size').find('option:selected').val());
                return valSale;
            }
            $("#product-size").change(function(){
                var valSale = parseInt($(this).find("option:selected").val());
                var valBuy = valSale - 20000;
                $("#product-price__buy").text(formatAngka(valBuy));
            });
            $("#product-size").change(function(){
                var valPrice = parseInt($(this).find("option:selected").val());
                $("#product-price__sale").text(formatAngka(valPrice));
            });
            function formatAngka(angka) {
                if (typeof(angka) != 'string') angka = angka.toString();
                var reg = new RegExp('([0-9]+)([0-9]{3})');
                while(reg.test(angka)) angka = angka.replace(reg, '$1.$2');
                return angka;
            }     
        },
        _formOrder = function () {
            var valWeight = parseInt($('#weightItem').attr("data-weight"));
            var valSale = parseInt($('#sizeItem').find('option:selected').val());
            var sizeText = $('#sizeItem').find("option:selected").text();
            $("#weightItem").val(formatAngka(valWeight)); 
            $("#size-item").val(sizeText);
            $("#priceItem").val(formatAngka(valSale));
            $("#price-item").val('Rp. ' + formatAngka(valSale));  
            $('.add_qty').on('click', function(ev) {
                var $currObj = $(ev.currentTarget);
                var currQCount = getCurrQCount($currObj);
                currQCount++;
                updateData($currObj, currQCount);
            });
            $('.remove_qty').on('click', function(ev) {
                var $currObj = $(ev.currentTarget);
                var currQCount = getCurrQCount($currObj);
                currQCount--;
                updateData($currObj, currQCount);
            }); 
            function getCurrQCount($currObj) {
                return $currObj.siblings(".input_qty, .simpleCart_quantity").val();
            }
            function updateData($currObj, currQCount) {
            $currObj.siblings(".input_qty").val(currQCount);
                var totalItem = getCurrQCount($currObj);
                var valWeight = parseInt($('#weightItem').attr("data-weight"));
                var valPrice = parseInt($('#sizeItem').find('option:selected').val());
                var subTotal = totalItem * valPrice ;
                var totalWeight = totalItem * valWeight;
                var valOngkir = parseInt($('#ongkir').val());
                var totalOngkir = valOngkir * (getOngkir() / 1000);
                var totalCost = subTotal + totalOngkir;
                $("#total-items").val(totalItem + ' pcs');
                $("#subtotal").val('Rp. ' + formatAngka(subTotal));
                $("#total-weight").val(formatAngka(totalWeight) + ' gram');
                $("#total-shipping").val('Rp. ' + formatAngka(totalOngkir));
                $("#total-cost").val('Rp. ' + formatAngka(totalCost));
                $("#order-cart").text('Rp. ' + formatAngka(totalCost));
                $(".order-info__checkout").addClass("checkout-show");

            }
            $("#kec").on('awesomplete-selectcomplete',function(){
                var valPrice = parseInt($('#sizeItem').find('option:selected').val());
                var totalItem = parseInt($('#total-item').val());
                var subTotal = totalItem * valPrice ;
                var valOngkir = parseInt($('#ongkir').val());
                var totalOngkir = valOngkir * (getOngkir() / 1000);
                var totalCost = subTotal + totalOngkir;
                $("#price-ongkir").text(formatAngka(valOngkir));
                $("#price-shipping").val('Rp. ' + formatAngka(valOngkir));
                $("#total-shipping").val('Rp. ' + formatAngka(totalOngkir));
                $("#total-cost").val('Rp. ' + formatAngka(totalCost));
                $("#order-cart").text('Rp. ' + formatAngka(totalCost));
                $(".order-info__checkout").addClass("checkout-show");
                $("#cek-ongkir").text(formatAngka(valOngkir));
            });
            $("#sizeItem").change(function(){
                var valPrice = parseInt($(this).find("option:selected").val());
                var totalItem = parseInt($('#total-item').val());
                var subTotal = totalItem * valPrice ;
                var totalWeight = totalItem * valWeight;
                var valOngkir = parseInt($('#ongkir').val());
                var totalOngkir = valOngkir * (getOngkir() / 1000);
                var totalCost = subTotal + totalOngkir;
                var sizeText = $(this).find("option:selected").text();
                $("#priceItem").val(formatAngka(valPrice));
                $("#price-item").val('Rp. ' + formatAngka(valPrice));
                $("#size-item").val(sizeText);
                $("#subtotal").val('Rp. ' + formatAngka(subTotal));
                $("#total-weight").val(formatAngka(totalWeight) + ' gram');
                $("#total-cost").val('Rp. ' + formatAngka(totalCost));
                $("#order-cart").text('Rp. ' + formatAngka(totalCost));
                $(".order-info__checkout").addClass("checkout-show");
            }); 
            function getOngkir() {
                var totalItem = parseInt($('#total-item').val());
                var totalWeight = valWeight * totalItem;
                var nilai = totalWeight;
                var pembulat = 1000;
                var hasil = (Math.ceil(parseInt(nilai))%parseInt(pembulat) == 0) ? Math.ceil(parseInt(nilai)) : Math.round((parseInt(nilai)+parseInt(pembulat)/2)/parseInt(pembulat))*parseInt(pembulat);
                return hasil;
            }
            function formatAngka(angka) {
                if (typeof(angka) != 'string') angka = angka.toString();
                var reg = new RegExp('([0-9]+)([0-9]{3})');
                while(reg.test(angka)) angka = angka.replace(reg, '$1.$2');
                return angka;
            }     
        },
        _fitVids = function () {
            // Start fitVids
            var $postContent = $(".tren-youtube");
            $postContent.fitVids();
            // End fitVids
        },
        _numFormat = function () {
            $('span.number').number( true );
        }

    return {
        init: function () {
            _slickSlider();
            _mobileMenu();
            _clickOrder();
            _selectPrice();
            _formOrder();
            _fitVids();
            _numFormat();
        }
    };

})(jQuery);

(function () {

    'use strict';
    hijacket.init();

})();